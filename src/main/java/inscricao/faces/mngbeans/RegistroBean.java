/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Matsunaga
 */
@ManagedBean(name = "registroBean")
@ApplicationScoped
public class RegistroBean extends utfpr.faces.support.PageBean {
    public static List<Candidato> candidatosList = new ArrayList<Candidato>();  

    public static List<Candidato> getCandidatosList() {
        return candidatosList;
    }

    public static void setCandidatosList(List<Candidato> candidatosList) {
        RegistroBean.candidatosList = candidatosList;
    }

    
    
    public RegistroBean() {
       
    }
}
